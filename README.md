# gaia

#Dependencies
Python3 is required to run the script
Required python dependencies are stored in requirements file

```
pip3 install -r requirements.txt

```
# Run app

Under directory name gaia run a command:

```
python3 start.py

```

1. Script is starting.

2. Start connecting with redis server for getting list of active servers.

3. After getting a list of server start connecting with every server one by one in list.

4. If connect with standard port i.e ssh port - 22 will send a success message on telegram bot channel and then disconnect server.

5. If can't connect with standard port try to connect with admin port i.e 9576. 

6. If server is connected successfuly with admin port then check the memory space and disk space.

7. If memory is less than 5% then reboot the server and if disk space is less than 5% then find the biggest file and remove this file.

8. If can't connect server with admin port also then send a message on telegram bot channel for invalid credentials and create a ticket on board through monday.com api's.

9. All Done.

