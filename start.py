from pexpect import pxssh
from module.redis_module import  Redis
from module.server_module import  Server
from module.telegram import  Telegram

# Start Script
print('Starting script now')

# defining percentage of threshold for memory and space
memory_threshold_perc = 5
space_threshold_perc = 5

# define redis and server objects 
redis=Redis()
server=Server()
telegram=Telegram()

# If can't connect to redis then return
if redis.connect() is None:
	print("Can't connect to redis server, so can't get list");
	exit

# get server list from redis
lists = redis.getList()

for serverConf in lists:
	# connect on standard port
	isConnected=server.connect(serverConf['host'],serverConf['port'])
	# if connected then continue to next server
	if isConnected is not None:
		print("Connected Successfully "+ serverConf['host'] + " on port "+ serverConf['port'])
		telegram.send_notifications("Connected Successfully "+ serverConf['host'] + " on port "+ serverConf['port'])
		server.disconnect()
		continue
	
	# if can't connect on standard port, send a notification on telegram bot and connect on admin port to check the problem 
	print("Can't Connect "+ serverConf['host'] + " on standard port trying to connect with admin port to check the problem")
	telegram.send_notifications("Can't Connect "+ serverConf['host'] + " on standard port trying to connect with admin port to check the problem")
	isAdminConnected=server.connect(serverConf['host'],serverConf['adminPort'])
	if isAdminConnected is None:
		message = "Not_connected_on_"+serverConf['host']
		server.addIssueInBoard(message,serverConf['host'])
		telegram.send_notifications("Can't connect "+serverConf['host']+" on admin port also. Please provide valid credentials for connecting server with admin port.")
		print("Can't connect "+serverConf['host']+" on admin port also. Please provide valid credentials for connecting server with admin port.")
		continue
	print("Connected successfully "+serverConf['host']+" on admin port: "+serverConf['adminPort'])	
	
	# check memory usage
	memory= server.getMemory()
	if ( ( memory.free / memory.total ) * 100) <= memory_threshold_perc:
		server.preformAction('free_memory')
	
	# check disk usage
	disk= server.getDiskSpace()
	if ( ( disk.free / disk.total ) * 100) <= space_threshold_perc:
		server.preformAction('free_disk')
	server.disconnect()
	
# All done
print('All Done')
