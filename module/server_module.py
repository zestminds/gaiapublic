from pexpect import pxssh
import getpass
import requests


class Server:
	host = "localhost"
	port = 22
	user = "root"
	password = "12345"
	url = "https://api.monday.com/v2/"
	server_obj = None
	
	def __init__(self):
		pass
	def disconnect(self):
		self.server_obj.logout()
		return self
	
	def connect(self,host,port):
		if not host: host = self.host
		if not port: port = self.port		
		if self.user: user = self.user		
		if self.password: password = self.password		
		try:
			self.server_obj = pxssh.pxssh()
			self.server_obj.force_password = True
			self.server_obj.login(host, user, password , port , auto_prompt_reset=False,original_prompt=r'@[^:]+:.*?/#?')
			return self
			
		except pxssh.ExceptionPxssh as e:
			return None
	
	def addIssueInBoard(self,msg,host):
		payload = "{\"query\": \"mutation{create_item(board_id:279565923,item_name:\\\""+msg+"\\\", column_values:\\\"{\\\\\\\"text\\\\\\\":\\\\\\\""+host+"\\\\\\\",\\\\\\\"status\\\\\\\": {\\\\\\\"index\\\\\\\": 2},\\\\\\\"status1\\\\\\\": {\\\\\\\"index\\\\\\\": 2},\\\\\\\"status8\\\\\\\": {\\\\\\\"index\\\\\\\":2},\\\\\\\"status9\\\\\\\": {\\\\\\\"index\\\\\\\":2}}\\\"){id}}\"}"
		headers = {
			'Authorization': "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjE5NDY3MjAzLCJ1aWQiOjkwNzk1MTEsImlhZCI6IjIwMTktMDgtMDYgMDY6MjQ6MjkgVVRDIiwicGVyIjoibWU6d3JpdGUifQ.hgsR4uln0vvCU41vGuaKTYYAWG6UNw_B1e9BNvM7GLg",
			'cache-control': "no-cache,no-cache",
			'Content-Type': "application/json"
			}
		response = requests.request("POST", self.url, data=payload, headers=headers)
		return response
	
	def getMemory(self):
		memory= self.server_obj.sendline("free -m")
		# ~ memory = convertTextIntoList(memory) // {free: 100, total: 1024}
		print(memory)
		return memory
		
	def getDiskSpace(self):
		space= self.server_obj.sendline("df -m");
		# ~ space = convertTextIntoList(space) // {free: 100, total: 1024 * 256}
		print(space)
		return space
		
	def performAction(self,action):
		if(action == 'free_memory'):
			self.server_obj.sendline("reboot")
		elif(action == 'free_disk'):
			file=self.server_obj.sendline("find bigfile");
			self.server_obj.sendline("rm "+ file.path);
		return self
