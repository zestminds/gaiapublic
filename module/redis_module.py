import redis
# ~ host = "localhost"
# ~ port = 6379

class Redis:
	host = "localhost"
	port = 6379
	redis_obj = None
	
	def __init__(self):
		pass
	
	def connect(self):
		if self.host: host = self.host
		if self.port: port = self.port
		try:
			self.redis_obj = redis.Redis(host, port)
			return self
		except:
			return None
			
	def getList(self):
		lists = self.redis_obj.smembers("gaia")
		serverLists = []
		for server_list in lists:
			list = server_list.decode("utf-8").split(":")
			list = { 'host':list[0] ,'port':list[1],'protocol':list[2],'adminPort':list[3] }
			serverLists.append(list)
		return serverLists
