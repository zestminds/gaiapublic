import requests

url = "https://api.monday.com/v2/"
print(url)
text = "test"
# ~ payload = '{\"query\":\"{boards{id,name}}\"}'
# ~ payload = '{\"mutation\":\"{create_item (board_id: 279565923, group_id: "today", item_name: "testing through python") {id}}\"}'
# ~ payload = "{\"query\":\"mutation{create_item(board_id:279565923,item_name:server_not_connected){id}}\"}"
payload = "{\"query\": \"mutation{create_item(board_id:279565923,item_name:"+text+", column_values:\\\"{\\\\\\\"text\\\\\\\":\\\\\\\"Testing\\\\\\\",\\\\\\\"status\\\\\\\": {\\\\\\\"index\\\\\\\": 1},\\\\\\\"status1\\\\\\\": {\\\\\\\"index\\\\\\\": 1},\\\\\\\"status8\\\\\\\": {\\\\\\\"index\\\\\\\":1},\\\\\\\"status9\\\\\\\": {\\\\\\\"index\\\\\\\":1}}\\\"){id}}\"}"
print(payload)
headers = {
    'Authorization': "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjE5NDY3MjAzLCJ1aWQiOjkwNzk1MTEsImlhZCI6IjIwMTktMDgtMDYgMDY6MjQ6MjkgVVRDIiwicGVyIjoibWU6d3JpdGUifQ.hgsR4uln0vvCU41vGuaKTYYAWG6UNw_B1e9BNvM7GLg",
    'cache-control': "no-cache,no-cache",
    'Content-Type': "application/json"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
